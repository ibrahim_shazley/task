﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class Bussiness
    {
        SampleEntities _Context;

        public Bussiness()
        {
            _Context = new SampleEntities();
        }

        public IEnumerable<AdminDTO> GetAllAdmins()
        {
            return new SampleEntities().Admins.SqlQuery("exec [dbo].[spGetAllAdmins]").ToList().Select(x => new AdminDTO
            {
                AdminId = x.Admin_Id,
                AdminName = x.Admin_Name,
            });
        }

        public IEnumerable<AdminDTO> GetAdminById(int id)
        {
            return new SampleEntities().Admins.SqlQuery($"exec [dbo].[sp_GetAdminById] {id}").ToList().Select(x => new AdminDTO
            {
                AdminId = x.Admin_Id,
                AdminName = x.Admin_Name,
            });
        }

        public bool Addadmin(string adminName, string adminPassword)
        {
            try
            {
                new SampleEntities().CNTs.SqlQuery($"exec [dbo].[sp_AddAdmin] {adminName},{1}").ToList();
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool DeleteAdmin(int AdminId)
        {
            try
            {
                new SampleEntities().CNTs.SqlQuery($"exec [dbo].[sp_DeleteAdmin] {AdminId}").ToList();
                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }
        public bool EditAdmin(string adminName, int adminid)
        {
            try
            {
                new SampleEntities().CNTs.SqlQuery($"exec [dbo].[sp_UpdateAdmin] {adminid},{adminName},{1}").ToList();
                return true;
            }
            catch (Exception ex) { return false; }
        }

    }
}
