﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class AdminDTO
    {
        public int AdminId { get; set; }
        public string AdminName { get; set; }
    }
}
