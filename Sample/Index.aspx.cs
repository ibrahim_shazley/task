﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL;
using System.Web.Services;
using Newtonsoft.Json;

namespace Sample
{
    public partial class Index : System.Web.UI.Page
    {
        public static Bussiness bussiness = new Bussiness ();
        JsonSerializer serializer = new JsonSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }


        [WebMethod()]
        public static string GetAlladmins()
        {
            return JsonConvert.SerializeObject(bussiness.GetAllAdmins());
        }

        [WebMethod]
        public static bool AddAdmin(string adminName, string adminPassword)
        {
            return bussiness.Addadmin(adminName,adminPassword);
        }


        [WebMethod]
        public static bool DeleteAdmin(int adminid)
        {
            return bussiness.DeleteAdmin(adminid);
        }

    }
}