﻿using BLL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sample
{
    public partial class EditAdmin : System.Web.UI.Page
    {
        public static Bussiness bussiness = new Bussiness();
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        [WebMethod]
        public static string GetAdmin(int adminid)
        {
            return JsonConvert.SerializeObject(bussiness.GetAdminById(adminid));
        }

        [WebMethod]
        public static bool EditAdmins(string adminName, int adminid)
        {
            return bussiness.EditAdmin(adminName, adminid);
        }
    }
}