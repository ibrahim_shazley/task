﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AddAdmin.aspx.cs" Inherits="Sample.AddAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="col">
            <div class="col-xs-4">
                <label>Admin Name</label>
                <input type="text" id="adminName" class="form-control" />
            </div>
            <div class="col-xs-5">
               <label>Password</label>
                <input type="password" id="adminPassword" class="form-control" />
            </div>
            <div class="col-xs-6">
                <button type="button" class="btn btn-primary" onclick="addAdmin()">Add New Admin</button>
            </div>
        </div>
    </div>

    <script>
        function addAdmin() {
            let adminName = $('#adminName').val();
            let adminPassword = $('#adminPassword').val();
            if (!adminName || !adminPassword) {
                alert('fill in required fields !');
            } else {
                jQuery.ajax({
                    url: 'index.aspx/AddAdmin',
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify({ adminName: adminName, adminPassword: adminPassword }),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        window.location.href = '/Index.aspx';
                       // loadAdmins();
                    }
                });
            }
        }
    </script>
</asp:Content>
