﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="EditAdmin.aspx.cs" Inherits="Sample.EditAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="col">
            <div class="col-xs-4">
                <label>Admin Name</label>
                <input type="text" id="adminName" class="form-control" />
            </div>
            <br />
            <div class="col-xs-6">
                <button type="button" class="btn btn-primary" onclick="editItem()">Update Admin</button>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function () {
            GetAdmin();
        });

       
    </script>
</asp:Content>
