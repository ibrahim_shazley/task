﻿function loadAdmins() {
    jQuery.ajax({
        url: 'index.aspx/GetAlladmins',
        type: "POST",
        dataType: "json",
        data: "",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            let items = JSON.parse(data.d);
            $('#AdminsTBL tbody').html('');
            for (var i = 0; i < items.length; i++) {
                let item = items[i];
                let _tr = '<tr>';
                _tr += '<td>' + item.AdminId + '</td>';
                _tr += '<td>' + item.AdminName + '</td>';
                _tr += '<td><a href="EditAdmin.aspx?id=' + item.AdminId + '">Edit</a></td>';
                _tr += '<td><a href="javascript:void(0)" onclick="deleteItem(' + item.AdminId + ')">Delete</a></td>';
                _tr += '</tr>';
                $('#AdminsTBL tbody').append(_tr);
            }
        }
    });
}



function GetAdmin() {
    jQuery.ajax({
        url: 'EditAdmin.aspx/GetAdmin',
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ adminid: Number(window.location.href.split('?id=')[1]) }),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            debugger
            $('#adminName').val(JSON.parse(data.d)[0].AdminName);
        }
    });
}


function editItem(adminid) {
    if (!adminName) {
        alert('fill in required field !');
    } else {
        jQuery.ajax({
            url: 'EditAdmin.aspx/EditAdmins',
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ adminName: $('#adminName').val(), adminid: Number(window.location.href.split('?id=')[1]) }),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                window.location.href = '/Index.aspx';
            }
        });
    }
}

function deleteItem(adminid) {
    debugger
    //   let del_id=$(this).attr(Id)
    jQuery.ajax({
        url: 'index.aspx/DeleteAdmin',
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ adminid: adminid }),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            loadAdmins();
        }
    });
}