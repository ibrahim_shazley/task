USE [master]
GO
/****** Object:  Database [Sample]    Script Date: 01/04/2019 03:16:42 م ******/
CREATE DATABASE [Sample]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Sample', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Sample.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Sample_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Sample_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Sample] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Sample].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Sample] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Sample] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Sample] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Sample] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Sample] SET ARITHABORT OFF 
GO
ALTER DATABASE [Sample] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Sample] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Sample] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Sample] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Sample] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Sample] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Sample] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Sample] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Sample] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Sample] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Sample] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Sample] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Sample] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Sample] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Sample] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Sample] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Sample] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Sample] SET RECOVERY FULL 
GO
ALTER DATABASE [Sample] SET  MULTI_USER 
GO
ALTER DATABASE [Sample] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Sample] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Sample] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Sample] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Sample] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Sample]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 01/04/2019 03:16:42 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[Admin_Id] [int] IDENTITY(1,1) NOT NULL,
	[Admin_Name] [nchar](10) NULL,
	[Rec_Status] [bit] NULL,
	[Ins_user] [int] NULL,
	[Ins_Date] [datetime] NULL,
	[Upd_User] [int] NULL,
	[Upd_date] [datetime] NULL,
	[Del_User] [int] NULL,
	[Del_date] [datetime] NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[Admin_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CNT]    Script Date: 01/04/2019 03:16:42 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CNT](
	[Id] [int] NOT NULL,
 CONSTRAINT [PK_CNT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[sp_AddAdmin]    Script Date: 01/04/2019 03:16:42 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_AddAdmin]
	@AdminName NVARCHAR(50),
	@User_Id INT
AS
BEGIN
	INSERT INTO dbo.Admin 
	        ( Admin_Name ,
	          Rec_Status ,
	          Ins_user ,
	          Ins_Date 
			
	        )
	VALUES  ( @AdminName , -- Admin_Name - nchar(10)
	          0 , -- Rec_Status - bit
	          @User_Id , -- Ins_user - int
	          GETDATE()  -- Ins_Date - datetime

	        )
	SELECT 1 AS Id
END;


GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAdmin]    Script Date: 01/04/2019 03:16:42 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_DeleteAdmin]
@AdminId int
AS
BEGIN
DELETE FROM dbo.Admin WHERE Admin_Id=@AdminId
SELECT 1 AS Id
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAdminById]    Script Date: 01/04/2019 03:16:42 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetAdminById] 
@id int
AS
BEGIN
	SELECT TOP 1 * FROM dbo.Admin WHERE @id=Admin_Id
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateAdmin]    Script Date: 01/04/2019 03:16:42 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_UpdateAdmin]
	@AdminId INT,
	@AdminName NVARCHAR(50),
	@User_Id INT
AS
BEGIN
	UPDATE dbo.Admin SET Admin_Name = @AdminName , Upd_User = @User_Id , Upd_date = GETDATE()
	WHERE Admin_Id = @AdminId
	SELECT 1 AS Id
END;

GO
/****** Object:  StoredProcedure [dbo].[spGetAllAdmins]    Script Date: 01/04/2019 03:16:42 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetAllAdmins]
as
begin
select * from [Admin]
end


GO
USE [master]
GO
ALTER DATABASE [Sample] SET  READ_WRITE 
GO
